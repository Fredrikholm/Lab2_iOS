//
//  DemosViewController.swift
//  ResumeApp
//
//  Created by Fredrik Holm on 2017-10-28.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

class DemosViewController: UIViewController {

    
    @IBOutlet weak var demoImage: UIImageView!
    var sendingCell = ""

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if sendingCell == "Demo 1" {
            let firstImageView = UIImageView(image: UIImage(named: "animationimage.jpeg"))
            firstImageView.frame = view.frame
            view.addSubview(firstImageView)
            
            imageFadeIn(imageView: firstImageView)
        }
    }
    
    func imageFadeIn(imageView: UIImageView) {
        
        let secondImageView = UIImageView(image: UIImage(named: "animationimagebarren.jpg"))
        secondImageView.frame = view.frame
        secondImageView.alpha = 0.0
        
        view.insertSubview(secondImageView, aboveSubview: imageView)
        
        UIView.animate(withDuration: 2.0, delay: 2.0, options: .curveEaseOut, animations: {
            secondImageView.alpha = 1.0
        }, completion: {_ in
            imageView.image = secondImageView.image
            secondImageView.removeFromSuperview()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}
