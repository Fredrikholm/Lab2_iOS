//
//  AddressViewController.swift
//  ResumeApp
//
//  Created by Fredrik Holm on 2017-10-28.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit
import MapKit

class AddressViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let location = CLLocationCoordinate2D(latitude: 57.798712, longitude: 14.148941)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Jönköping"
        annotation.subtitle = "Sweden"
        mapView.addAnnotation(annotation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
