//
//  WorksViewController.swift
//  ResumeApp
//
//  Created by Fredrik Holm on 2017-10-28.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit

class WorksViewController: UIViewController {

    @IBOutlet weak var worksImage: UIImageView!
    @IBOutlet weak var worksLabel: UILabel!
    var sendingCell = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if sendingCell == "JTH - Education" {
            worksImage.image = UIImage(named: "jth.jpg")
            worksLabel.text = "JTH Education, Software Development: 2015 - ongoing"
        }
        else if sendingCell == "DataKraft - Intern" {
            worksImage.image = UIImage(named: "dk.jpg")
            worksLabel.text = "Internship, DataKraft AB: 2 months during 2017"
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
