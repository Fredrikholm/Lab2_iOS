//
//  WebsiteViewController.swift
//  ResumeApp
//
//  Created by Fredrik Holm on 2017-10-28.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit
import WebKit

class WebsiteViewController: UIViewController, WKNavigationDelegate {

     var webView: WKWebView!
    // for showing a website
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
        
        let url = URL(string: "https://facebook.com")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
