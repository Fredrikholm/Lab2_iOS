//
//  ViewController.swift
//  ResumeApp
//
//  Created by Fredrik Holm on 2017-10-26.
//  Copyright © 2017 Fredrik Holm. All rights reserved.
//

import UIKit
import MessageUI

class NameImageCell: UITableViewCell {
    
    @IBOutlet weak var firstCellImage: UIImageView!
    @IBOutlet weak var firstCellLabel: UILabel!
}

class PhoneCell: UITableViewCell {
    
    @IBOutlet weak var secondCellButton: UIButton!
}

class MailCell: UITableViewCell {
    
    @IBOutlet weak var thirdCellButton: UIButton!
}

class AddressCell: UITableViewCell {
    
    @IBOutlet weak var fourthCellButton: UIButton!
}

class WebCell: UITableViewCell {
    
    @IBOutlet weak var fifthCellButton: UIButton!
}

class WorkTableCell: UITableViewCell {
    
    @IBOutlet weak var workCellButton: UIButton!
}

class DemoTableCell: UITableViewCell {
    
    @IBOutlet weak var demoCellButton: UIButton!
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
   
    
    // Variables for handling works and demos
    let sections = [" ", "Work Experience", "Demos"]
    let works = ["JTH - Education", "DataKraft - Intern"]
    let demos = ["Demo 1"]
    var firstSectionCellCounter = 0
    // temporary arrays for populating cells, when these are empty, all of the cells have been populated
    var tempWorks: Array = [""]
    var tempDemos: Array = [""]
    var tempSections: Array = [""]

        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        tempWorks = works
        tempDemos = demos
        tempSections = sections
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 5
        case 1:
            return works.count
        case 2:
            return demos.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
        
    }// Default is 1 if not implemented

    
    
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return ""
        } else if section == 1 {
            return "Work Experiences"
            
        } else {
            return "Demos"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "worksSegue") {
            let vc = segue.destination as! WorksViewController
            if let sendingButtonTitle = (sender as? UIButton)?.titleLabel?.text {
                vc.sendingCell = sendingButtonTitle
            }
        }
        else if (segue.identifier == "demosSegue") {
            let vc = segue.destination as! DemosViewController
            if let sendingButtonTitle = (sender as? UIButton)?.titleLabel?.text {
                vc.sendingCell = sendingButtonTitle
            }            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionNumber = indexPath.section
        
        
        if sectionNumber == 0 {
            switch firstSectionCellCounter {
            case 0:
                firstSectionCellCounter += 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameImageCell") as! NameImageCell
                cell.firstCellImage.image = UIImage(named: "fredrik.jpeg")
                cell.firstCellLabel.text = "Fredrik Holm"
                return cell
            case 1:
                firstSectionCellCounter += 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell") as! PhoneCell
                cell.secondCellButton.setTitle("Give me a call!", for: UIControlState.normal)
                return cell
            case 2:
                firstSectionCellCounter += 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "mailCell") as! MailCell
                cell.thirdCellButton.setTitle("Mail me!", for: UIControlState.normal)
                return  cell
            case 3:
                firstSectionCellCounter += 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell") as! AddressCell
                // configure cell
                cell.fourthCellButton.setTitle("Find me!", for: UIControlState.normal)
                return cell
            case 4:
                firstSectionCellCounter += 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "webCell") as! WebCell
                // configure cell
                cell.fifthCellButton.setTitle("My fave!", for: UIControlState.normal)
                return cell
            
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "")
                return cell!
            }
            // Add the correct cells
        } else if sectionNumber == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "workTableCell") as! WorkTableCell
            cell.workCellButton.setTitle(tempWorks[0], for: UIControlState.normal)
            tempWorks.remove(at: 0)
            return cell
            // add the correct cells
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "demoTableCell") as! DemoTableCell
            cell.demoCellButton.setTitle(tempDemos[0], for: UIControlState.normal)
            if tempDemos.count > 1 {tempDemos.remove(at: 0)}
            return cell
            // add the correct cells
            
        }
    }
    
    // "OWN" FUNCTIONS
    func configureMailController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["fredde_holm@hotmail.se"])
        mailComposerVC.setSubject("Sent from your application!")
        mailComposerVC.setMessageBody("Contacting you with...", isHTML: false)
        
        return mailComposerVC
    }
    
    
    // BUTTONS_CLICK
    
    @IBAction func phoneButton_Click(_ sender: UIButton) {
        let url: NSURL = URL(string: "TEL://0730250983")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func mailButton(_ sender: UIButton) {
        if !MFMailComposeViewController.canSendMail() {
            print("Cannot access mail services!")
            return
        }
        let mailComposeViewController = configureMailController()
        self.present(mailComposeViewController, animated: true, completion: nil)
    }

    
   @IBAction func workButton(_ sender: UIButton) {
        performSegue(withIdentifier: "worksSegue", sender: sender)
    
    }
    
    @IBAction func demoButton(_ sender: UIButton) {
        performSegue(withIdentifier: "demosSegue", sender: sender)
    }
}
